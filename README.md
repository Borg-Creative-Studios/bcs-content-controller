# BCS Content Controller

Manage HTML across websites with JS.

BCS Content Controller is designed to be a simpler way of managing website content.

We are currently working on a documentation for this project to make using it easy for developers and non-developers.

You can see BCS Content Controller in action [here](https://www.openteanews.com/).

Load order: HTML -> JavaScript -> JavaScript generated HTML/CSS
